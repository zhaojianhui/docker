##docker卸载

###centos上卸载

####1首先搜索已经安装的docker 安装包 
```bash
[root@localhost ~]# yum list installed|grep docker 
```
或者使用该命令 
```bash
[root@localhost ~]# rpm -qa|grep docker 
docker.x86_64 2:1.12.6-16.el7.centos @extras 
docker-client.x86_64 2:1.12.6-16.el7.centos @extras 
docker-common.x86_64 2:1.12.6-16.el7.centos @extra
```

2 分别删除安装包 
```bash
[root@localhost ~]#yum remove –y docker.x86_64 
[root@localhost ~]#yum remove –y docker-client.x86_64 
[root@localhost ~]#yum remove –y docker-common.x86_64 
```

3 删除docker 镜像 
```bash
[root@localhost ~]# rm -rf /var/lib/docker 
```

4 再次check docker是否已经卸载成功 
```bash
[root@localhost ~]# rm -rf /var/lib/docker 
[root@localhost ~]# 
```
如果没有搜索到，那么表示已经卸载成功。