### centos7下安装

### 参考中国区官网文档[https://www.docker-cn.com/]

1.yum update #更新yum源
```bash```
sudo yum install -y yum-utils \
  device-mapper-persistent-data \
  lvm2

# docker官方
sudo yum-config-manager \
--add-repo \
https://download.docker.com/linux/centos/docker-ce.repo

# 阿里云
sudo yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo

sudo yum install docker-ce docker-ce-cli containerd.io #安装docker
```
2.设置开机自启动
```bash
sudo systemctl enable docker
```
3.替换国内镜像源
```bash
sudo mkdir -p /etc/docker
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://qqe07tk2.mirror.aliyuncs.com"]
}
EOF
sudo systemctl daemon-reload
sudo systemctl restart docker
```
4.启动docker服务
```bash
sudo systemctl restart docker
sudo systemctl status docker.service
```

5.进入正在运行中的容器
```sh
sudo docker exec -it 775c7c9ee1e1 /bin/bash
```