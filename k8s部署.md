#在CentOS 7上安装Kubernetes的步骤

要使用Kubernetes，您需要安装一个容器化引擎。当前，最受欢迎的容器解决方案是Docker。码头工人需要在CentOS的安装，这两个主节点和工作节点。

参考网址：
https://blog.csdn.net/twingao/article/details/105382305

##步骤1：系统环境。
```shell script
cat /etc/redhat-release

uname -a

sudo vi /etc/hosts
192.168.56.100 k8s-master
192.168.56.101 k8s-node1
192.168.56.102 k8s-node2
```

关闭防火墙和安全设置。
```shell script
systemctl stop firewalld
systemctl disable firewalld


vi /etc/fstab
#/dev/mapper/centos-swap swap                    swap    defaults        0 0

vi /etc/selinux/config
SELINUX=disabled

#重启生效
reboot
```

配置免密码登录。在每个节点都执行ssh-keygen产生公私钥对，都选缺省值，一路回车即可。
````shell script
ssh-keygen
````
用ssh-copy-id将本节点的公钥复制到其它节点，如k8s-master节点，需要将公钥复制到k8s-node1、k8s-node2节点，其它节点都要类似操作。
```shell script
ssh-copy-id k8s-node1
ssh-copy-id k8s-node2
```
修改内核参数。
```shell script
cat <<EOF> /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF

modprobe br_netfilter

sysctl --system

* Applying /usr/lib/sysctl.d/00-system.conf ...
net.bridge.bridge-nf-call-ip6tables = 0
net.bridge.bridge-nf-call-iptables = 0
net.bridge.bridge-nf-call-arptables = 0
* Applying /usr/lib/sysctl.d/10-default-yama-scope.conf ...
kernel.yama.ptrace_scope = 0
* Applying /usr/lib/sysctl.d/50-default.conf ...
kernel.sysrq = 16
kernel.core_uses_pid = 1
net.ipv4.conf.default.rp_filter = 1
net.ipv4.conf.all.rp_filter = 1
net.ipv4.conf.default.accept_source_route = 0
net.ipv4.conf.all.accept_source_route = 0
net.ipv4.conf.default.promote_secondaries = 1
net.ipv4.conf.all.promote_secondaries = 1
fs.protected_hardlinks = 1
fs.protected_symlinks = 1
* Applying /etc/sysctl.d/99-sysctl.conf ...
* Applying /etc/sysctl.d/k8s.conf ...
#注意需要有以下两行
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
* Applying /etc/sysctl.conf ...
```

安装Docker。
```shell script
yum install -y yum-utils device-mapper-persistent-data lvm2

yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
或者
#yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo

yum list docker-ce --showduplicates | sort -r

yum install -y docker-ce-19.03.8-3.el7

systemctl start docker
systemctl enable docker

docker version
```
设置Kubernetes的yum源。
```shell script
sudo cat <<EOF> /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64/
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg https://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
EOF
```
或
```shell script
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=http://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=http://packages.cloud.google.com/yum/doc/yum-key.gpg http://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF
```
>注意：如果使用sudo命令，则不仅将其附加到cat命令，还应将其附加到受限制的文件。

##步骤2：安装kubelet，kubeadm和kubectl
要使用Kubernetes，需要这3个基本软件包。在每个节点上安装以下软件包：
```shell script
yum list -y kubeadm --showduplicates

sudo yum install -y kubelet kubeadm kubectl
kubeadm version
sudo systemctl enable kubelet && sudo systemctl start kubelet
```
您现在已经成功安装了Kubernetes，包括其工具和基本软件包。


在部署群集之前，请确保设置主机名，配置防火墙和内核设置。

##步骤3：在节点上设置主机名
要为每个节点提供唯一的主机名，请使用以下命令：
```shell script
sudo hostnamectl set-hostname k8s-master
```
或者
```shell script
sudo hostnamectl set-hostname k8s-node1
sudo hostnamectl set-hostname k8s-node2
```
在此示例中，主节点现在命名为k8s-master，而工作节点则命名为k8s-node1和k8s-node2

所有节点设置主机或DNS记录以解析的主机名：
```shell script
sudo vi /etc/hosts
```
输入
```shell script
192.168.56.100 k8s-master
192.168.56.101 k8s-node1
192.168.56.102 k8s-node2
```

##步骤4：配置防火墙

节点，容器和Pod需要能够跨集群通信以执行其功能。默认情况下，前端的CentOS中启用Firewalld。通过输入列出的命令来添加以下端口。

在主节点上执行：
```shell script
sudo firewall-cmd --permanent --add-port=6443/tcp
sudo firewall-cmd --permanent --add-port=2379-2380/tcp
sudo firewall-cmd --permanent --add-port=10250/tcp
sudo firewall-cmd --permanent --add-port=10251/tcp
sudo firewall-cmd --permanent --add-port=10252/tcp
sudo firewall-cmd --permanent --add-port=10255/tcp
sudo firewall-cmd –-reload
```
每次添加端口时，系统都会以“成功”消息进行确认。

在每个工作节点上输入以下命令：
```shell script
sudo firewall-cmd --permanent --add-port=10251/tcp
sudo firewall-cmd --permanent --add-port=10255/tcp
firewall-cmd –-reload
```

##步骤5：更新Iptables设置
在sysctl配置文件中将net.bridge.bridge-nf-call-iptables设置为“ 1”。这样可以确保IP表在过滤和端口转发过程中正确处理数据包。
```shell script
cat  < /etc/sysctl.d/master_node_name
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
sudo sysctl --system
```

##步骤6：停用SELinux
容器需要访问主机文件系统。SELinux需要设置为宽松模式，这实际上会禁用其安全功能。

使用以下命令禁用SELinux：
```shell script
sudo setenforce 0
sudo sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config
```

##步骤7：停用SWAP
最后，我们需要禁用SWAP才能使kubelet正常工作：
```shell script
sudo sed -i '/swap/d' /etc/fstab
sudo swapoff -a
```

sudo systemctl start etcd
sudo systemctl start docker
sudo systemctl start kube-apiserver
sudo systemctl start kube-controller-manager
sudo systemctl start kube-scheduler
sudo systemctl start kubelet
sudo systemctl start kube-proxy

#如何部署Kubernetes集群

##第1步：设置Pod网络
Pod网络允许群集中的节点进行通信。本教程为此目的使用flannel虚拟网络附加组件。

使用以下命令安装flannel：
```shell script
sudo kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
```

##第2步：使用kubeadm创建集群
为了使flannel正常工作，请执行以下命令来初始化集群：
```shell script
sudo kubeadm init --kubernetes-version=1.18.3 \
--pod-network-cidr=10.244.0.0/16 \
--service-cidr=10.1.0.0/16 \
--ignore-preflight-errors=cri \
--apiserver-advertise-address=${MASTER_PRIVATE_IP} \
--apiserver-cert-extra-sans=${MASTER_PUBLIC_IP} \
--image-repository registry.aliyuncs.com/google_containers

#---------------

sudo kubeadm init --kubernetes-version=1.18.3 \
--pod-network-cidr=10.244.0.0/16 \
--service-cidr=10.1.0.0/16 \
--ignore-preflight-errors=cri \
--apiserver-advertise-address=192.168.56.100 \
--image-repository registry.aliyuncs.com/google_containers
```
根据网络速度，此过程可能需要几分钟才能完成。该命令完成后，将显示kubeadm加入消息。记下该条目。它有助于我们在稍后阶段将工作程序节点加入集群。

####重置
```shell script
sudo kubeadm reset
```


##步骤3：以普通用户身份管理群集
通过输入以下内容，开始使用需要以普通用户身份运行的集群：
```shell script
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```
修改Service的nodePort缺省取值范围。
```shell script
sudo vi /etc/kubernetes/manifests/kube-apiserver.yaml
- --service-node-port-range=1-39999

sudo systemctl daemon-reload
sudo systemctl restart kubelet
```


##步骤4：检查群集状态
安装Pod网络后，您可以通过键入以下内容来检查CoreDNS Pod是否正在运行，以确认其是否正常运行：
```shell script
kubectl get pods --all-namespaces
kubectl get nodes
kubectl get all -n kube-system
```
查看端口号
```shell script
sudo netstat -tunlp
```

部署flannel网络。
```shell script
wget https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
sudo cp /data/wwwroot/kube-flannel.yml ~/
```
以上地址可能无法访问，可以直接下载代码仓库https://github.com/coreos/flannel，解压flannel-master.zip文件，将flannel-master\Documentation\kube-flannel.yml上传到k8s-master节点。

建议将kube-flannel.yml中的kube-flannel-ds-arm64、kube-flannel-ds-arm、kube-flannel-ds-ppc64le和kube-flannel-ds-s390x这4个daemonset删除，只保留kube-flannel-ds-amd64，因为一般的实验环境都是amd64架构的。
可以提前将镜像拉下来，四个节点都操作。
```shell script
sudo docker pull quay.io/coreos/flannel:v0.12.0-amd64
sudo kubectl apply -f ~/kube-flannel.yml
```

##步骤5：将Worker节点加入集群
如步骤2所示，您可以在每个工作节点上输入kubeadm join命令以将其连接到集群。
```shell script
sudo kubeadm join 192.168.56.100:6443 --token odib2i.el1mort56tp1faok     --discovery-token-ca-cert-hash sha256:79ab86122075a070d89d4b89b47b73b5e39803995849033385bd039ba42f896f --v=5

kubeadm join 10.0.2.15:6443 --token t4y6qq.prhefhwk07xnbe2j \
    --discovery-token-ca-cert-hash sha256:ac93cd01db78defb57f5339865fd672d737c664b4b62f8856aa73cefeddb0576  
```
将代码替换为主服务器中的代码。对集群上的每个工作节点重复此操作。现在检查节点的状态。

从主服务器输入：
```shell script
sudo kubectl get nodes
sudo netstat -tunlp
```
系统现在显示您加入集群的新工作节点。

#结论
您已经在CentOS上成功安装了Kubernetes，现在可以跨多个服务器管理集群。如果您有裸机服务器，则可能需要查看我们的指南，了解如何在此类服务器上安装Kubernetes。

该Kubernetes教程为探索这个多功能平台必须提供的许多选项提供了一个很好的起点。使用Kubernetes可以更高效地扩展您的运营，并在微管理容器上花费更少的时间。

对于仍然没有部署多个容器的经验的初学者，  Minikube  是一个不错的入门方法。 Minikube  是一个用于在本地运行单节点集群的系统，非常适合在学习Kubernetes之前学习基础知识。
