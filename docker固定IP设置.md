# Docker固定IP设置

经常用Docker模拟项目在生产环境中的部署，往往需要同时开好几台Docker容器，而且有时安装的软件需要绑定Docker局域网中的其他容器，如 MongoDB 副本集部署的时候，就需要绑定其他容器的内网IP。

但是，Docker 每次重启后，容器的IP地址会变化，查询了资料，Docker是支持设置固定IP的。

### Docker 默认网络
Docker安装后，默认会创建下面三种网络类型：

```bash
docker network ls


NETWORK ID          NAME                DRIVER              SCOPE
64f68bc46756        bridge              bridge              local
488b6c3f3f25        host                host                local
6b171140c966        none                null                local
```

启动 Docker的时候，用`--network` 参数，可以指定网络类型，如：
```bash
docker run -itd --name test1 --network bridge --ip 172.17.0.10 centos:latest /bin/bash
```

#### bridge：桥接网络
默认情况下启动的Docker容器，都是使用 bridge，Docker安装时创建的桥接网络，每次Docker容器重启时，会按照顺序获取对应的IP地址，这个就导致重启下，Docker的IP地址就变了
#### none：无指定网络
使用 `--network=none` ，docker 容器就不会分配局域网的IP
#### host： 主机网络
使用 `--network=host`，此时，Docker 容器的网络会附属在主机上，两者是互通的。
例如，在容器中运行一个Web服务，监听8080端口，则主机的8080端口就会自动映射到容器中。

###创建自定义网络：（设置固定IP）
启动Docker容器的时候，使用默认的网络是不支持指派固定IP的，如下：
```bash
docker run -itd  --net bridge --ip 172.17.0.10 centos:latest /bin/bash

57b10319120b9210692de938c11df03b4dc611a3581255519da81ec68a63f238
/usr/bin/docker-current: Error response from daemon: User specified IP address is supported on user defined networks only.
```
因此，需要创建自定义网络，下面是具体的步骤：

#### 步骤1: 创建自定义网络
创建自定义网络，并且指定网段：172.18.0.0/16

```bash
docker network create --subnet=172.18.0.0/16 mynetwork
docker network ls

NETWORK ID          NAME                DRIVER              SCOPE
64f68bc46756        bridge              bridge              local
488b6c3f3f25        host                host                local
44bae5b38edb        mynetwork           bridge              local
6b171140c966        none                null                local
```
#### 步骤2: 创建Docker容器

```bash
docker run -itd --name networkTest1 --net mynetwork --ip 172.18.0.2 centos:latest /bin/bash
```
这个时候，创建的Docker容器就会持有 172.18.0.2 这个IP.

```bash
ifconfig
```

### 显示所有容器IP地址

```bash
docker inspect --format='{{.Name}} - {{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(docker ps -aq)
```

//nginx
```bash
docker run -itd --name nginx --net mynetwork --ip 172.18.0.2 -v /data/wwwroot/:/data/wwwroot/ -v /data/wwwroot/conf.d/:/etc/nginx/conf.d/ -v /data/nginx/log/:/var/log/nginx/ -v /data/nginx/logs/:/etc/nginx/logs/ -p 80:80 -d nginx
```

//php
```bash
docker build -t=zjh/php:7.4-fpm /data/wwwroot/docker/php7.4-fpm-simple/
docker run -itd --name php --net mynetwork --ip 172.18.0.3 -v /data/wwwroot/:/data/wwwroot/ -d zjh/php:7.4-fpm
```

//官方原版启动（5.7）
```bash
docker run -itd --name mysql57 -e MYSQL_ROOT_PASSWORD=123456 -e MYSQL_DATABASE=test -e MYSQL_USER=qianxun -e MYSQL_PASSWORD=123456 -v $HOME/.data/mysql57:/var/lib/mysql -p 3357:3306 -d mysql:5.7
```

//官方原版启动（8.0）
```bash
docker run -itd --name mysql80 -e MYSQL_ROOT_PASSWORD=123456 -e MYSQL_DATABASE=test -e MYSQL_USER=qianxun -e MYSQL_PASSWORD=123456 -v $HOME/.data/mysql80:/var/lib/mysql -p 3306:3306 -d hub.suofeiya.com/docker-hub-proxy/library/mysql:8 --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci
```



### 启动redis容器,主机6379端口，容器6379

```bash
docker run -itd --name redis -v $HOME/.data/redis:/data -p 6379:6379 hub.suofeiya.com/docker-hub-proxy/library/redis:alpine redis-server --appendonly yes --requirepass "123456"
```

### 通过docker执行composer

```bash
###当前目录
docker run --rm --interactive --tty --volume $PWD:/app  --volume ${COMPOSER_HOME:-$HOME/.composer}:/tmp  --user $(id -u):$(id -g)  composer install --ignore-platform-reqs --no-scripts --no-plugins

###绝对路径
docker run --rm --interactive --tty --volume /data/website/laravelOpenWechat/:/app composer install --ignore-platform-reqs
```

### 通过docker指定npm脚本

```bash
#安装
docker run -it --rm --name my-running-script -v "$PWD":/usr/src/app -w /usr/src/app node:8 npm install
#编译
docker run -it --rm --name my-running-script -v "$PWD":/usr/src/app -w /usr/src/app node:8 npm run prod
```

### 使用nginx+php集成版镜像启动容器

```shell script
#https://gitlab.com/ric_harvey/nginx-php-fpm
sudo docker run -itd --name nginx-php -e 'WEBROOT=/var/www/html/src/' -v /data/wwwroot/:/var/www/html/ -p 80:80 richarvey/nginx-php-fpm:latest
```

### 本地启动开发环境

```shell script
#在wsl虚拟机中运行环境
docker run -itd --name php72 --net mynetwork --ip 172.18.0.1 -p 80:80 -v /home/qianxun/website/:/data/wwwroot:cached -v /home/qianxun/wwwlogs:/data/wwwlogs -v /home/qianxun/vhost:/usr/local/nginx/conf/vhost/ -d hub.suofeiya.com/marketing-php-base/php7.2-full

docker run -itd --name php74 --net mynetwork --ip 172.18.0.2 -p 80:80 -v /home/qianxun/website/:/data/wwwroot:cached -v /home/qianxun/wwwlogs:/data/wwwlogs -v /home/qianxun/vhost:/usr/local/nginx/conf/vhost/ -d hub.suofeiya.com/marketing-php-base/php7.4-full

docker run -itd --name php80 --net mynetwork --ip 172.18.0.3 -p 80:80 -v /home/qianxun/website/:/data/wwwroot:cached -v /home/qianxun/wwwlogs:/data/wwwlogs -v /home/qianxun/vhost:/usr/local/nginx/conf/vhost/ -d hub.suofeiya.com/marketing-php-base/php8.0-full

#在wsl虚拟机中运行环境(阿里云)
docker run -itd --name php56 --net mynetwork --ip 172.18.0.5 -p 80:80 -v /home/qianxun/website/:/data/wwwroot:cached -v /home/qianxun/wwwlogs:/data/wwwlogs -v /home/qianxun/vhost:/usr/local/nginx/conf/vhost/ -d registry.cn-shenzhen.aliyuncs.com/zhaojianhui/php5.6-full

docker run -itd --name php72-redis4 --net mynetwork --ip 172.18.0.1 -p 80:80 -v /home/qianxun/website/:/data/wwwroot:cached -v /home/qianxun/wwwlogs:/data/wwwlogs -v /home/qianxun/vhost:/usr/local/nginx/conf/vhost/ -d registry.cn-shenzhen.aliyuncs.com/zhaojianhui/php7.2-full-redis4

docker run -itd --name php72 --net mynetwork --ip 172.18.0.2 -p 80:80 -v /home/qianxun/website/:/data/wwwroot:cached -v /home/qianxun/wwwlogs:/data/wwwlogs -v /home/qianxun/vhost:/usr/local/nginx/conf/vhost/ -d registry.cn-shenzhen.aliyuncs.com/zhaojianhui/php7.2-full

docker run -itd --name php74 --net mynetwork --ip 172.18.0.3 -p 80:80 -v /home/qianxun/website/:/data/wwwroot:cached -v /home/qianxun/wwwlogs:/data/wwwlogs -v /home/qianxun/vhost:/usr/local/nginx/conf/vhost/ -d registry.cn-shenzhen.aliyuncs.com/zhaojianhui/php7.4-full

docker run -itd --name php80 --net mynetwork --ip 172.18.0.4 -p 80:80 -v /home/qianxun/website/:/data/wwwroot:cached -v /home/qianxun/wwwlogs:/data/wwwlogs -v /home/qianxun/vhost:/usr/local/nginx/conf/vhost/ -d registry.cn-shenzhen.aliyuncs.com/zhaojianhui/php8.0-full

#hyperf项目
docker run --name hyperf -v d:/website/hyperf-skeleton:/hyperf-skeleton -p 9501:9501 -it --entrypoint /bin/sh hyperf/hyperf:latest
php /hyperf-skeleton/bin/hyperf.php start
```

### 快捷命令

```shell
# tyarn14
tyarn14 () {
    docker run \
        --rm \
        -it \
        -v $HOME/.node/:/usr/local/share/.cache/ -v $PWD:/usr/src/app \
        -w /usr/src/app \
        hub.suofeiya.com/marketing-php-base/node:14-alpine tyarn "$@"
}

# tyarn16 使用示例：tyarn16 -p8080:8000
# 此处 -p 参数与容器运行时 -p 参数等价, 不传入时默认为 8000:8000 
# 若需定义其他参数可仿照添加(不建议添加过多自定义参数，违背了快捷使用的初衷)
tyarn16 () {
    # 默认使用 8000 端口
    port="8000:8000"
    GETOPT_ARGS=`getopt -o p: -al port: -- "$@"`
    eval set -- "$GETOPT_ARGS"
    while [ -n "$1" ]
        do
            case "$1" in
                -p|--port) port=$2; shift 2;;
                --) param=$2; break ;;
            esac
    done

    docker run \
        --rm \
        -it \
        -v $HOME/.node/:/usr/local/share/.cache/ -v $PWD:/usr/src/app \
        -p $port \
        -w /usr/src/app \
        hub.suofeiya.com/marketing-php-base/node:16-alpine tyarn $param
}

# composer1.0
composer1 () {
    extra=
    [ "$@" ] && extra="--ignore-platform-reqs --no-scripts" # 判断输入参数是否为空
    docker run \
        --rm \
        -it \
        -v $HOME/.composer/:/tmp/ \
        -v $PWD:/app \
        hub.suofeiya.com/marketing-php-base/composer:1 composer "$@" $extra
}

# composer2.0
composer2 () {
    extra=
    [ "$@" ] && extra="--ignore-platform-reqs --no-scripts" # 判断输入参数是否为空
    docker run \
        --rm \
        -it \
        -v $HOME/.composer/:/tmp/ \
        -v $PWD:/app \
        hub.suofeiya.com/marketing-php-base/composer:2 composer "$@" $extra
}
```



